package main

import (
	"errors"
	"os"

	"github.com/hashicorp/vault/api"
)

func NewVaultClient() (*api.Client, error) {
	config := api.DefaultConfig()
	cli, err := api.NewClient(config)
	if err != nil {
		return nil, err
	}
	cli.SetToken(os.Getenv("VAULT_TOKEN"))
	return cli, nil
}

func ListKeys(cli *api.Client, path string) ([]string, error) {
	secret, err := cli.Logical().List(path)
	if err != nil {
		return nil, err
	}
	paths, _, err := listPath(secret)
	return paths, err
}

func ListSecrets(cli *api.Client, path string) ([]string, error) {
	secret, err := cli.Logical().List(path)
	if err != nil {
		return nil, err
	}
	_, secrets, err := listPath(secret)
	return secrets, err
}

func listPath(secret *api.Secret) ([]string, []string, error) {
	if secret == nil || secret.Data == nil || secret.Data["keys"] == nil {
		return nil, nil, errors.New("secret keys not found")
	}
	keys := secret.Data["keys"].([]interface{})
	var (
		paths   []string
		secrets []string
	)
	for _, k := range keys {
		record := k.(string)
		if record[len(record)-1] == '/' {
			paths = append(paths, k.(string))
		} else {
			secrets = append(secrets, k.(string))
		}
	}
	return paths, secrets, nil
}
