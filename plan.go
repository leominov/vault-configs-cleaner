package main

import (
	"context"
	"os"
	"path"
	"sort"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	excludedBranches = map[string]struct{}{
		"master": {},
	}

	planCmd = &cobra.Command{
		Use:   "plan",
		Short: "Create plan",
		RunE: func(cmd *cobra.Command, args []string) error {
			vaultClient, err := NewVaultClient()
			if err != nil {
				return err
			}

			kubeClient, err := NewKubernetesClient()
			if err != nil {
				return err
			}

			logrus.Infof("Collecting configs list from Vault...")
			services, err := ListKeys(vaultClient, "configs/metadata")
			if err != nil {
				return err
			}

			var (
				configsMap   = make(map[string][]string)
				configsTotal int
			)
			for _, service := range services {
				service = strings.Trim(service, "/")
				branches, err := ListKeys(vaultClient, path.Join("configs/metadata", service))
				if err != nil {
					logrus.Error(err)
					continue
				}
				if _, ok := configsMap[service]; !ok {
					configsMap[service] = []string{}
				}
				for _, branch := range branches {
					branch = strings.Trim(branch, "/")
					configsTotal++
					configsMap[service] = append(configsMap[service], branch)
				}
			}
			logrus.Infof("Found %d configs", configsTotal)

			logrus.Infof("Collecting releases list from Kubernetes...")
			configMapList, err := kubeClient.CoreV1().ConfigMaps("").List(context.TODO(), metav1.ListOptions{})
			if err != nil {
				return err
			}

			var (
				releasesMap   = make(map[string][]string)
				releasesTotal int
			)
			for _, cm := range configMapList.Items {
				service := cm.Labels["name"]
				branch := cm.Labels["branch"]
				if service == "" || branch == "" {
					continue
				}
				if _, ok := releasesMap[service]; !ok {
					releasesMap[service] = []string{}
				}
				releasesTotal++
				releasesMap[service] = append(releasesMap[service], branch)
			}
			logrus.Infof("Found %d releases", releasesTotal)

			var deleteList []string
			for service, branches := range configsMap {
				servicePath := path.Join("configs/metadata", service)
				kubeBranches, ok := releasesMap[service]
				if !ok {
					logrus.Warnf("Service %q with %d configs not found in Kubernetes", service, len(branches))
					for _, branch := range branches {
						branchPath := path.Join(servicePath, branch)
						keys, err := ListSecrets(vaultClient, branchPath)
						if err != nil {
							logrus.Error(err)
							continue
						}
						for _, key := range keys {
							deleteList = append(deleteList, path.Join(branchPath, key))
						}
					}
					continue
				}
				for _, branch := range branches {
					var match bool
					if _, ok := excludedBranches[branch]; ok {
						continue
					}
					for _, kubeBranch := range kubeBranches {
						if kubeBranch == branch {
							match = true
							break
						}
					}
					if !match {
						logrus.Errorf("Release %q of service %q not found in Kubernetes", branch, service)
						branchPath := path.Join(servicePath, branch)
						keys, err := ListSecrets(vaultClient, branchPath)
						if err != nil {
							logrus.Error(err)
							continue
						}
						for _, key := range keys {
							deleteList = append(deleteList, path.Join(branchPath, key))
						}
					}
				}
			}

			sort.Strings(deleteList)
			data := strings.Join(deleteList, "\n")
			err = os.WriteFile("plan.out", []byte(data), 0644)
			return err
		},
	}
)
