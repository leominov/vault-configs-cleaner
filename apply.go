package main

import (
	"bufio"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	applyCmd = &cobra.Command{
		Use:   "apply",
		Short: "Apply plan",
		RunE: func(cmd *cobra.Command, args []string) error {
			vaultClient, err := NewVaultClient()
			if err != nil {
				return err
			}
			file, err := os.Open("plan.out")
			if err != nil {
				return err
			}
			scanner := bufio.NewScanner(file)
			for scanner.Scan() {
				path := scanner.Text()
				logrus.Infof("Deleting %s...", path)
				_, err := vaultClient.Logical().Delete(path)
				if err != nil {
					logrus.WithError(err).Error("Failed to delete")
				}
			}
			return scanner.Err()
		},
	}
)
