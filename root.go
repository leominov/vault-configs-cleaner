package main

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:          "vault-configs-cleaner",
	SilenceUsage: true,
}

func init() {
	rootCmd.AddCommand(
		planCmd,
		applyCmd,
	)
}
