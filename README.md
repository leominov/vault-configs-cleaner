# Vault Configs Cleaner

Удаляет данные из kv `configs` на основе опубликованных релизов Kubernetes. Для работы необходимо задать `VAULT_ADDR` и `VAULT_TOKEN`.

## Команды

### plan

Генерирует файл `plan.out` со списком секретов для удаления из Vault.

### apply

Построчно обрабатывает файл `plan.out` и удаляет данные из Vault.
